package com.paditech.mvpbase.screen.apkManage;

import com.paditech.mvpbase.common.mvp.fragment.FragmentPresenterViewOps;
import com.paditech.mvpbase.common.mvp.fragment.FragmentViewOps;

/**
 * Created by hung on 4/14/2018.
 */

public interface ApkContact {

    interface ViewOps extends FragmentViewOps{

    }

    interface PresenterViewOps extends FragmentPresenterViewOps{

    }
}

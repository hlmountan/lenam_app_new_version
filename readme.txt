/*
 Cấu trúc của project
 hoạt động và chỉnh sửa
*/

/*
  Mô hình Model - view - Presenter
  Mỗi activity sẽ đi kèm với 1 controller + 1 Presenter
  activity lấy dữ liệu từ API bằng cách gọi presenter thông qua controller
  controller là 1 interface bao goom2 interface con là presenterOPS và ViewOPS 
  PresenterOPS dc presenter implement còn ViewOPS dc Activity hoặc fragment implement
  Present cần extend ViewOPS và activity hoặc fragment cần extend PresenterOPS để có thể giao tiếp được với nha
*/

/*
  View gọi thực thi presenter bằng cú pháp getPresenter().Function(); (một hàm nàm đó trong PresenterOPS)
  nhận dữ liệu bằng việc implement các interface của ViewOPS 
*/

/*
  Gọi API
  
  Presenter gọi API bằng APIClient.getInstance()...
  dữ liệu trả về  trong  onResponse là 1 string JSON sau đó parse JSON bằng thư viện GSON 
  Dữ liệu đưa đến View bằng cách gọi hàm getView().FunctionInViewOPS()   (1 hàm nào đó trong ViewOPS)
  
*/